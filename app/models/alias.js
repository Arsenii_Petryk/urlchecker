const mongoose = require('mongoose');

const aliasShema = new mongoose.Schema({
  chatId: Number,
  sequence: { type: Number, default: 0 }
});

aliasShema.statics.getAlias = function getAlias(chatId) {
  return this.findOneAndUpdate(
    { chatId },
    { $inc: { sequence: 1 } },
    { upsert: true, new: true });
};

module.exports = mongoose.model('alias', aliasShema);

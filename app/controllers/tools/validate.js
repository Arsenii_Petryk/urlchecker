class Validate {
  constructor() {
    this.properties = new Set(['headers', 'method', 'statuscodes', 'bodyschema', 'token', 'interval']);
    this.statuses = new Set([101, 102, 200, 201, 202, 203, 204, 205, 206, 207, 226, 300, 301, 302, 302, 303,
      304, 305, 306, 307, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411,
      412, 413, 414, 415, 416, 417, 422, 423, 424, 425, 426, 428, 429, 431, 434, 444,
      449, 451, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511
    ]);
    this.methods = new Set(['OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'TRACE', 'CONNECT']);
  }

  property(string) {
    return this.properties.has(string);
  }

  integers(string) {
    return /^\d+$/i.test(string);
  }

  statusCodes(array) {
    return array.every && array.every(code => this.statuses.has(code));
  }

  token(string) {
    return /^[\w\d]+$/i.test(string);
  }

  method(string) {
    return this.methods.has(string.toUpperCase());
  }

  json(string) {
    try {
      JSON.parse(string);
    } catch (e) {
      return false;
    }
    return true;
  }
  aliases(string) {
    return /^(\d[ ,]?)+$/.test(string)
  }
}

module.exports = new Validate();

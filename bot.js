const bot = require('./bootstrap');
const routes = require('./app/routes');
const events = require('./app/events');
module.exports = bot;

routes.forEach(route => bot.onText(route.command, require(`./app/controllers/${ route.controller }`)));
events.forEach(event => bot.on(event.event, require(`./app/controllers/${ event.controller }`)));
require('./app/worker');

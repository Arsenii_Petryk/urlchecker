const validUrl = require('valid-url');
const urls = require('../models/url');
const worker = require('../worker');
const sanitize = require('./tools/sanitize');

module.exports = ({ sendMessage, param1, param2, chatId }) => {
  const alias = sanitize.alias(param1);
  let interval = param2;
  if (interval) {
    interval = sanitize.interval(param2);
    if (!interval.valid) {
      return sendMessage(interval.result);
    }
    interval = interval.result;
  }
  if (validUrl.isUri(param1)) {
    return worker.watchNewUrl(urls.createOrUpdate({ interval, chatId, url: param1 }));
  }
  if (alias.valid) {
    return worker.watchNewUrl(urls.findOneAndUpdate({ chatId, alias: alias.result }, { active: true }, { new: true }));
  }
  return sendMessage('not valid url or alias');
};

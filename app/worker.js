const request = require('request');
const sendMessage = require('../bot').sendMessage;
const urls = require('./models/url');
const StatsD = require('node-statsd');
const inspector = require('schema-inspector');
const client = new StatsD({
  host: process.env.STATS_HOST,
  port: process.env.STATS_PORT
});

class Worker {
  constructor() {
    this.watchers = {};
    this.onInit();
  }

  onInit() {
    this.memoryCheck();
    urls.find({ active: true })
      .then(activeUrls => activeUrls.forEach(this.watch))
      .catch(err => console.log(err));
  }

  memoryCheck() {
    setInterval(
      () => client.histogram('Bot_memory', process.memoryUsage().rss),
      process.env.INTERVAL_OF_MEMORY_CHECK
    );
  }

  formatMessage(url) {
    return `start watching url '${ url.alias }': ${ url.url }`;
  }

  watchNewUrl(dBRequest) {
    return dBRequest.then(url => {
      sendMessage(url.chatId, this.formatMessage(url));
      this.watch(url);
    }).catch(err => console.log(err));
  }

  watch(url) {
    if (url.bodyschema) {
      url.bodyschema = JSON.parse(url.bodyschema);
    }
    this.watchers[url._id] = {
      cicle: setInterval((uri) => this.check(uri), url.interval, url)
    };
  }

  cutUrl(url) {
    return url.match(/(?::\/\/)(.+)/i)[1];
  }

  check(url) {
    const startTime = Date.now();
    url.callback = (error, req, body) => {
      client.timing(this.cutUrl(url.url), Date.now() - startTime);
      if (error) {
        return sendMessage(url.chatId, `unable to check this url: ${ url.url }`);
      }
      if (this.invalidStatusCode(req, url.statuscodes)) {
        tsendMessage(url.chatId, `${ url.url } - statusCode is ${ req.statusCode }`);
      }
      if (url.bodyschema) {
        try {
          const result = inspector.validate(url.bodyschema, JSON.parse(body));
          if (!result.valid) {
            sendMessage(url.chatId,
              `body validation '${ url.alias }' url: ${ url.url }:
              logs: ${ result.format }`
            );
          }
        } catch (e) {
          sendMessage(url.chatId, 'unable to parse body');
        }
      }
    };
    this.makeRequest(url);
  }

  invalidStatusCode(req, statusCodes) {
    return statusCodes.some(code => req.statusCode !== code);
  }

  makeRequest({ url, headers, method, token, callback, body }) {
    const options = { url, method, headers, body };
    if (token) {
      options.auth = { bearer: token };
    }
    request(options, callback);
  }

  unwatch(url) {
    clearInterval(this.watchers[url._id].cicle);
    delete this.watchers[url._id];
  }

  reset(url) {
    this.unwatch(url);
    this.watch(url);
  }
}

client.socket.on('error', error => console.error('Error in socket: ', error));

module.exports = new Worker();

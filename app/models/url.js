const mongoose = require('mongoose');
const aliases = require('./alias');

const urlSchema = new mongoose.Schema({
  chatId: Number,
  alias: Number,
  url: String,
  body: String,
  bodyschema: String,
  token: String,
  user: String,
  password: String,
  headers: String,
  headersschema: String,
  interval: {
    type: Number,
    default: 300000
  },
  active: {
    type: Boolean,
    default: true
  },
  method: {
    type: String,
    default: 'GET'
  },
  statuscodes: {
    type: Array,
    default: [200]
  }
}
// , { timestamps: true}
);

urlSchema.statics.createOrUpdate = function createOrUpdate({ interval, chatId, url }) {
  return this.findOne({ url, chatId })
  .then(newUrl => {
    if (newUrl) {
      newUrl.active = true;
      interval && (newUrl.interval = interval);
      return newUrl.save();
    }
    return this.create({ url, chatId, interval });
  });
};

urlSchema.pre('save', function pre(next) {
  if (!this.alias) {
    aliases.getAlias(this.chatId)
      .then(alias => {
        this.alias = alias.sequence;
        next();
      });
  } else {
    next();
  }
});

module.exports = mongoose.model('url', urlSchema);

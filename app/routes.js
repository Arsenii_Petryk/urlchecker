module.exports = [{
  controller: 'list',
  command: /\/list/
}, {
  controller: 'help',
  command: /\/(help)|\/(set|get) --help/
}, {
  controller: 'unwatch',
  command: /\/unwatch (.+)/
}, {
  controller: 'watch',
  command: /\/watch (\S+) ?(\d+)?/
}, {
  controller: 'set',
  command: /\/set (?!--help)([\w\d]+) ([\w\d]+) (.+)/
}, {
  controller: 'get',
  command: /\/get (?!--help)([\w\d]+) ([\w\d]+)/
}, {
  controller: 'export',
  command: /\/export (.+)/
}];

const urls = require('../models/url');

function formatMessage(urlList) {
  return urlList.map(url => `'${url.alias}': ${url.url} ||| ${url.active && 'active' || 'not active'} ||| interval ${ url.interval / 60000 } min.`).join('\n');
}

module.exports = ({ chatId, sendMessage }) => {
  urls.find({ chatId })
    .then(urlList => sendMessage((urlList.length) ? formatMessage(urlList) : "you haven't any urls to check"))
    .catch(error => console.log(error));
};

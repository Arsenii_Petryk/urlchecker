const TelegramBot = require('node-telegram-bot-api');
module.exports.Mybot = class Mybot extends TelegramBot {
  constructor(BOT_KEY) {
    super(BOT_KEY, { polling: true });
  }

  on(event, controller) {
    const callback = msg => {
      const sendMessage = message => this.sendMessage(msg.from.id, message);
      const downloadFile = () => this.downloadFile(msg.document.file_id, './');
      controller({
        config: msg.document,
        chatId: msg.from.id,
        downloadFile,
        sendMessage
      });
    };
    super.on(event, callback);
  }

  onText(command, controller) {
    const callback = (msg, match) => {
      const sendMessage = message => this.sendMessage(msg.from.id, message);
      const sendDocument = file => this.sendDocument(msg.from.id, file);
      controller({
        chatId: msg.from.id,
        param1: match[1],
        param2: match[2],
        param3: match[3],
        sendDocument,
        sendMessage
      });
    };
    super.onText(command, callback);
  }
};

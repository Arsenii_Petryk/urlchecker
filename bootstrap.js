const mongoose = require('mongoose');
const Mybot = require('./app/config').Mybot;
mongoose.connect(process.env.DB_HOST);
module.exports = new Mybot(process.env.BOT_KEY);

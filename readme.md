# Quick Start:

The [docker](https://docs.docker.com/engine/installation/) and [docker-compose](https://docs.docker.com/engine/installation/) must be installed.

1. clone: `$ git clone https://Arsenii_Petryk@bitbucket.org/Arsenii_Petryk/urlchecker.git && cd urlchecker`
2. Open Telegram
3. Enter "BotFather" in search field
4. Enter `/newbot` and then `NameOfYour_bot` (It must end in `bot`)
5. Copy answer token ( looks: `123456789:Q1W2e3r4t5R6E7T8Y9U0i`)
6. Open `docker-compose.yml` in your text editor and enter token instead of `-----Your BotFather newbot token-----`
7. Run `$ docker-compose up`

Then find your telegram bot by searching its name. To get commands you access, enter `/help` to your bot.

## Additional option

You can change checking memory interval of your bot-app by changing `INTERVAL_OF_MEMORY_CHECK` in `docker-compose.yml`

## Grafana

Open <http://localhost:3003>

```
Username: root
Password: root
```

### Add data source on Grafana

1. Open `Data Sources` from left side menu, then click on `Add data source`
2. Choose a `name` for the source and flag it as `Default`
3. Choose `InfluxDB` as `type`
4. Choose `direct` as `access`
5. Fill remaining fields as follows and click on `Add` without altering other fields

```
Url: <http://localhost:8086>
Database:    datasource
User: datasource
Password:    datasource
```

Now you are ready to add your first dashboard and launch some query on database.

FROM node:slim
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install --production
COPY . /app
EXPOSE 8080
CMD [ "node", "bot" ]

const messages = {
  help: `commands:
"/watch <url or alias> [<minutes>]" - to add url to check list, minutes - optional, default 5 min.
"/unwatch <url or alias>" - to remove url from check list
"/set <alias> <property> <value>" - to set property
"/set --help" - available properties
"/list" - to get all urls from check list`,
  set: `command: set <alias> <property> <value>
  available <property> - <value> type:
    headers - json
    method - 
    statuscode
    bodyschema
    token
    interval
    body
  get: `ge
  t`
};
const getHelpMessage = ``;
const setHelpMessage = ``;
module.exports = ({ sendMessage, param1, param2 }) => {
  !param1 && (param1 = param2);
  messages[param1] && sendMessage(messages[param1]);
};

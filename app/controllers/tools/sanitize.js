const validate = require('./validate');

function sunitizeStatus(codes) {
  let valid = validate.integers(codes);
  if (valid) {
    codes = [parseInt(codes, 10)];
  }
  if (!valid) {
    valid = validate.json(codes);
    codes = (valid) ? JSON.parse(codes) : codes.split(',').map(str => parseInt(str, 10));
  }
  valid = validate.statusCodes(codes);
  const result = valid && codes || 'invalid status codes';
  return {
    valid,
    result
  };
}

function runSunitize(string, property, log, callback) {
  const valid = validate[property](string);
  const result = valid && ((callback && callback(string)) || string) || log;
  return {
    valid,
    result
  };
}

module.exports = {
  interval: string => runSunitize(string, 'integers', 'invalid interval', int => parseInt(int, 10) * 60000),
  alias: string => runSunitize(string, 'integers', 'invalid alias', int => parseInt(int, 10)),
  method: string => runSunitize(string, 'method', 'invalid method', name => name.toUpperCase()),
  property: string => runSunitize(string, 'property', 'invalid propetry'),
  bodyschema: string => runSunitize(string, 'json', 'invalid schema'),
  headers: string => runSunitize(string, 'json', 'invalid headers'),
  token: string => runSunitize(string, 'token', 'invalid token'),
  aliases: string => runSunitize(string, 'aliases', 'invalid aliases',
    aliases => aliases.split(/[\s,]+/).map(str => parseInt(str, 10))),
  statuscodes: codes => sunitizeStatus(codes),
};

const urls = require('../models/url');
const json = require('jsonfile');
const fs = require('fs');
const worker = require('../worker');
const Convert = require('./tools/convertSwaggerToUrls');

function formatMessage(newUrls) {
  const formattedUrls = newUrls.map(url => `'${ url.alias }': ${ url.url }`)
    .join('\n');
  return `start watching:\n${ formattedUrls }`;
}

function stringify(downloadFile, callback) {
  let pathname;
  return downloadFile
    .then(path => {
      pathname = path;
      return json.readFile(path, (error, configs) => callback(configs));
    })
    .then(() => fs.unlink(pathname))
    .catch(err => console.log(err));
}

function createAndWatch(urlsToSave) {
  return urls.create(urlsToSave)
    .then(newUrls => newUrls.forEach(url => worker.watch(url)) || newUrls);
}

function mapUrls(urlsToMap, chatId) {
  return urlsToMap.map(url => (url.chatId = chatId) && url);
}

function importUrls({ chatId, downloadFile, sendMessage, callback }) {
  stringify(downloadFile(), configs => callback(configs)
    .then(urlsToMap => mapUrls(urlsToMap, chatId))
    .then(createAndWatch)
    .then(newUrls => sendMessage(formatMessage(newUrls))));
}

module.exports = args => {
  const { config } = args;
  if (config.file_name === 'urlConfig.json') {
    args.callback = configs => new Promise(resolve => resolve(configs));
    importUrls(args);
  }
  if (config.file_name === 'swagger.json') {
    const convert = new Convert();
    args.callback = configs => convert.swaggerToUrls(configs);
    importUrls(args);
  }
};

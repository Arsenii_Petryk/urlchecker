const urls = require('../models/url');
const worker = require('../worker');
const sanitize = require('./tools/sanitize');

const findAndSet = ({ sendMessage, alias, property, value, chatId }) => {
  const option = {};
  option[property] = value;
  return urls.findOneAndUpdate({ chatId, alias }, option, { new: true })
    .then(url => {
      worker.reset(url);
      sendMessage(`${ url.url } ${ property } is set`);
    })
    .catch(error => {
      sendMessage('try again later, I catch some data base error');
      console.log(error);
    });
};

module.exports = ({ sendMessage, param1, param2, param3, chatId }) => {
  const alias = sanitize.alias(param1);
  if (alias.valid) {
    const property = sanitize.property(param2);
    if (property.valid) {
      const value = sanitize[param2](param3);
      if (value.valid) {
        return findAndSet({
          alias: alias.result,
          value: value.result,
          property: param2,
          sendMessage,
          chatId
        });
      }
      return sendMessage(value.result);
    }
    return sendMessage(property.result);
  }
  return sendMessage(alias.result);
};

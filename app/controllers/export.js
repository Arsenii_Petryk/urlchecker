const json = require('jsonfile');
const urls = require('../models/url');
const sanitize = require('./tools/sanitize');

function prepareUrls(config, aliases) {
  return config.filter(url => aliases.has(url.alias)).map(fields => {
    const { url, interval, headers, method, statuscodes, bodyschema, token } = fields;
    return { url, interval, headers, method, statuscodes, bodyschema, token };
  });
}

module.exports = ({ chatId, sendMessage, param1, sendDocument }) => {
  const alias = sanitize.aliases(param1);
  if (alias.valid) {
    return urls.find({ chatId }).then(url => {
      if (url) {
        const urlsConfig = prepareUrls(url, new Set(alias.result));
        json.writeFile('./urlConfig.json', urlsConfig, error => {
          if (error) {
            return console.log(error);
          }
          return sendDocument('./urlConfig.json');
        });
      } else {
        sendMessage('not found some aliases');
      }
    }).catch(error => {
      console.log(error);
      sendMessage('try again later, I catch some data base error');
    });
  }
  return sendMessage(alias.result);
};

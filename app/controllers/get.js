const urls = require('../models/url');
const sanitize = require('./tools/sanitize');

module.exports = ({ chatId, sendMessage, param1, param2 }) => {
  const alias = sanitize.alias(param1);
  if (alias.valid) {
    const property = sanitize.propetry(param2);
    if (property.valid) {
      return urls.find({ chatId, alias: alias.result }).then(url => {
        if (url) {
          sendMessage(`${ url[param2] }`);
        } else {
          sendMessage('invalid alias');
        }
      }).catch(error => {
        console.log(error);
        sendMessage('try again later, I catch some data base error');
      });
    }
    return sendMessage(property.result);
  }
  return sendMessage(alias.result);
};

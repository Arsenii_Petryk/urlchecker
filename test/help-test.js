/* eslint-env node, mocha */
require('dotenv').config({
  path: '../.env'
});
const expect = require('chai').expect;
const urls = require('../app/models/url');
const worker = require('../app/worker');
const controllers = {};
['list', 'help', 'unwatch', 'watch', 'set', 'get', 'export'].forEach(controllerName => {
  controllers[controllerName] = require(`../app/controllers/${ controllerName }`);

  describe('/help', function () {
    describe('custom', function () {
      const helpResult = controllers.help({
        sendMessage: message => message
      });
      it('should return custom help message on /help command', function (done) {
        expect(helpResult).to.equal(`commands:
  "/watch <url or alias> [<minutes>]" - to add url to check list, minutes - optional, default 5 min.
  "/unwatch <url or alias>" - to remove url from check list
  "/set <alias> <property> <value>" - to set property
  "/set --help" - available properties
  "/list" - to get all urls from check list`);
        done();
      });
    });
    describe('/set --help', function () {
      const setHelp = controllers.help({
        sendMessage: message => message,
        param1: '/set'
      });
      it('should return help message on /set --help', function (done) {
        expect(setHelp).to.be.a('string');
        done();
      });
    });
    describe('/get --help', function () {
      const getHelp = controllers.help({
        sendMessage: message => message,
        param1: '/get'
      });
      it('should return help message on /get --help', function (done) {
        expect(getHelp).to.be.a('string');
        done();
      });
    });
  });
});

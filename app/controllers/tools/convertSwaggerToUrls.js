'use strict';
const util = require('util');
module.exports = class Convert {
  constructor(configs) {
    // this.coutn = 0;
    this.urls = [];
    this.promises = [];
    this.toDelete = [
      'xml', 'description', 'default',
      'additionalProperties', 'enum', '$ref',
      'example', 'required', 'format'
    ];
    this.handlers = {
      path: args => this.formatPath(args),
      formData: args => this.setFormData(args),
      body: args => this.setBody(args),
      query: args => this.formatQuery(args),
      header: args => this.setHeaders(args)
    };
    this.queryHandlers = {
      integer: args => this.setInteger(args),
      array: args => this.setArray(args),
      string: args => this.setString(args),
      file: args => this.setFile(args)
    };
  }

  swaggerToUrls(configs) {
    this.configs = configs;
    this.paths = configs.paths;
    this.definitions = configs.definitions;
    this.configs.schemes.forEach(schema => {
      this.host = `${ schema }://${ this.configs.host }${ this.configs.basePath || '' }`;
      this.forEach(this.paths, 'path', 'pathName', {}, args => {
        this.forEach(args.path, 'method', 'methodName', args, args1 => {
          this.forEach(args1.method.responses, 'response', 'statuscodes', args1, args2 => {
            // if (args2.method.consumes) {
            //   this.forEach(args2.method.consumes, 'ContentType', 'consumes', args2, args3 => {
            //     this.promises.push(this.addUrl(args3));
            //   });
            //   return;
            // }
            this.promises.push(this.addUrl(args2));
          });
        });
      });
    });
    return Promise.all(this.promises)
      .then(() => this.stringify(this.urls))
      .catch(error => console.log(error));
  }

  addUrl(properties) {
    properties = Object.assign({}, properties);
    return (new Promise(resolve => resolve(properties)))
      .then(args => this.createUrl(args))
      .then(args => this.setParameters(args));
  }

  stringify(urls) {
    urls.forEach(url => {
      ['body', 'bodyschema', 'headers'].forEach(key => url[key] && (url[key] = JSON.stringify(url[key])));
    });
    return urls;
  }

  forEach(obj, property, propertyName, args, callback) {
    args = Object.assign({}, args);
    !util.isArray(obj) && util.isObject(obj) && Object.keys(obj).forEach(key => {
      args[property] = obj[key];
      args[propertyName] = key !== 'default' && key || '200';
      this.checkDafault(args);
      callback(args);
    });
  }

  checkDafault({ responses, statuscodes }) {
    if (responses && !responses['200']) {
      responses['200'] = {};
      delete responses.default;
    }
  }

  setParameters(args) {
    const { method } = args;
    method.parameters.forEach(parameter => {
      args.parameter = parameter;
      this.handlers[parameter.in](args);
    });
    this.urls.push(args.url);
  }

  createUrl(args) {
    // console.log(this.coutn++, args.methodName, args.pathName, args.statuscodes);
    const { response, ContentType } = args;
    args.url = this.generateUrlfields(args);
    response.schema && (args.url.bodyschema = this.parse(response.schema));
    args.url.headers = {
      Accept: 'application/json',
      ContentType: 'application/json'
      // ContentType
    };
    return args;
  }

  generateUrlfields({ pathName, statuscodes, methodName }) {
    return {
      url: `${ this.host }${ pathName }`,
      method: methodName.toUpperCase(),
      statuscodes: [parseInt(statuscodes, 10)]
    };
  }

  setHeaders({ url, parameter }) {
    url.headers[parameter.name] = this.parseBody(parameter);
  }

  setFormData({ url, parameter }) {
    url.body = {};
    url.body[parameter.name] = this.defineProperty(arguments[0]);
  }

  parse(schema) {
    if (schema.$ref) {
      Object.assign(schema, this.parse(this.definitions[this.cutShemaPath(schema.$ref)]));
    }
    if (schema.type === 'array') {
      schema.items = this.parse(schema.items);
    }
    if (schema.format === 'date-time') {
      schema.pattern = schema.format;
    }
    if (schema.additionalProperties) {
      schema.properties = schema.additionalProperties;
    }
    if (schema.enum) {
      schema.pattern = schema.enum;
    }
    Object.keys(schema).forEach((key) => {
      !(schema[key] instanceof Array) && schema[key] instanceof Object && this.parse(schema[key]);
    });
    this.clean(schema);
    return schema;
  }

  setBody({ parameter, url, }) {
    if (parameter.schema.type === 'array') {
      url.body = [this.parseBody(this.definitions[this.cutShemaPath(parameter.schema.items.$ref)])];
      return;
    }
    url.body = this.parseBody(this.definitions[this.cutShemaPath(parameter.schema.$ref)]);
  }

  parseBody(schema) {
    if (schema.$ref) {
      return this.parseBody(this.definitions[this.cutShemaPath(schema.$ref)]);
    }
    if (schema.type === 'object') {
      return this.parseBody(schema.properties);
    }
    if (schema.type === 'array') {
      if (schema.items.$ref) {
        return [this.parseBody(this.definitions[this.cutShemaPath(schema.items.$ref)])];
      }
      if (schema.items.enum) {
        return [schema.items.enum[0]];
      }
      return [this.parseBody(schema.items)];
    }
    if (schema.type === 'string') {
      if (schema.format === 'date-time') {
        return new Date();
      }
      return 'string';
    }
    if (schema.type === 'integer') {
      return 1;
    }
    if (schema.type === 'boolean') {
      return true;
    }
    const url = {};
    Object.keys(schema).forEach((key) => {
      url[key] = this.parseBody(schema[key]);
    });
    return url;
  }

  clean(schema) {
    this.toDelete.forEach(property => delete schema[property]);
  }

  formatPath({ url }) {
    url.url = url.url.replace(/{\S+}/i, this.defineProperty(arguments[0]));
  }

  cutShemaPath(path) {
    return path.match(/\/(\w+)$/)[1];
  }

  defineProperty({ parameter }) {
    // console.log(parameter);
    return this.queryHandlers[parameter.type](arguments[0]);
  }

  setInteger() {
    return 1;
  }
  setArray({ parameter }) {
    if (parameter.items.enum) {
      return parameter.items.enum[0];
    }
    arguments[0].parameter = parameter.items;
    return this.defineProperty(arguments[0]);
  }
  setString({ parameter }) {
    if (parameter.format === 'date-time') {
      return new Date();
    }
    return 'string';
  }
  setFile() {
    return 'file';
  }
  formatQuery({ url, parameter }) {
    const query = this.defineProperty(arguments[0]);
    const and = /=/.test(url.url) && '?' || '&';
    if (!~url.url.indexOf(parameter.name)) {
      url.url += `${ and }${ parameter.name }=${ query }`;
    }
  }

};

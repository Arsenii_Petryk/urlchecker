/* eslint-env node, mocha */
require('dotenv').config({
  path: '../.env'
});
const expect = require('chai').expect;
const urls = require('../app/models/url');
const worker = require('../app/worker');
const controllers = {};
['list', 'help', 'unwatch', 'watch', 'set', 'get', 'export'].forEach(controllerName => {
  controllers[controllerName] = require(`../app/controllers/${ controllerName }`);
});

describe('controllers', function () {
  describe('/watch', function () {
    describe('add url to watch', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('add url with interval', function () {
      it('restart watching url whith new interval', function (done) {
        done();
      });
    });
    describe('watch url of alias', function () {
      it('change active field status and pass url to worker', function (done) {
        done();
      });
    });
  });
  describe('/unwatch', function () {
    describe('set disabled "active" url property and pass url to worker', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('remove url of alias from worker and set "active" to false', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
  });
  describe('/set', function () {
    describe('interval', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('method', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('property', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('bodyschema', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('headers', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('token', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('body', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
  });
  describe('/get', function () {
    describe('interval', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('method', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('property', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('bodyschema', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('headers', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('token', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
    describe('body', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
  });
  describe('/list', function () {
    describe('add url to watch', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
  });
  describe('/import', function () {
    describe('add url to watch', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
  });
  describe('/export', function () {
    describe('add url to watch', function () {
      it('add url to worker', function (done) {
        done();
      });
    });
  });
});

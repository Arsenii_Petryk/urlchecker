const validUrl = require('valid-url');
const urls = require('../models/url');
const worker = require('../worker');
const sanitize = require('./tools/sanitize');

module.exports = ({ chatId, param1, sendMessage }) => {
  const conditions = { chatId };
  const alias = sanitize.alias(param1);
  if (alias.valid) {
    conditions.alias = alias.result;
  }
  if (validUrl.isUri(param1)) {
    conditions.url = param1;
  }
  if ((!conditions.url && !conditions.alias)) {
    return sendMessage('not valid input');
  }
  return urls.findOneAndUpdate(conditions, { active: false })
    .then(url => {
      if (url) {
        sendMessage(`stop checking url: '${ url.url }'`);
        return worker.unwatch(url);
      }
      return sendMessage('not found such url');
    })
    .catch(error => {
      sendMessage('try again later, I catch some data base error');
      console.log(error);
    });
};
